### This repository for training ###

This branch for module6.

### Task 1 ###

Run all command line by line for task1:  
git config --global user.name "Oleg Kurnitsov"  
git config --global user.email a.kurnitsou@gmail.com  
git init issoft_training  
cd issoft_training  
cp /opt/share/nginx.conf nginx.conf  
chmod 664 nginx.conf  
cp /opt/share/README.md README.md  
chmod 664 README.md  
git add nginx.conf  
git commit -m "added nginx.conf"  
git add README.md  
git commit -m "added README.md"  
git log  
git status  
git remote add origin https://akurnitsou@bitbucket.org/akurnitsou/issoft_training.  
git push -u origin master  
cd ..  
rm -rf issoft_training  
cp /opt/share/issoft_ssh_key /home/vagrant/.ssh/id_rsa  
chmod 400 /home/vagrant/.ssh/id_rsa  
git clone git@bitbucket.org:akurnitsou/issoft_training.git  
cd issoft_training  
touch .gitignore  
echo "tmp" >> .gitignore  
git add .gitignore  
git commit -m "added gitignore"  
nano nginx.conf  
git add nginx.conf  
git commit -m "updated nginx.conf"  
git push -u origin master  
git log  
git log --oneline --graph --decorate --all  
git checkout b14dbae  
git tag -a v0.1  
git log --oneline --graph --decorate --all  
git status  
git checkout 64f2670  
git push -u origin master --tags  

### Task 2 ###

git checkout -b develop  
git checkout master  
git checkout -b feature/new-site  
git checkout develop  
nano nginx.conf  
git add .  
git commit -m "updated worker_connection"  
nano nginx.conf  
git add .  
git commit -m "updated gzip"  
git checkout feature/new-site   
mkdir conf.d  
touch conf.d/mysite.domain.com.conf  
nano conf.d/mysite.domain.com.conf  
git add .  
git commit -m "added custom site"  
git push origin feature/new-site  
git checkout develop  
git push origin develop  
git checkout master  
git pull origin master  
git merge -m "merge dev to master" develop  
git push origin master  
![First merge](first_merge.png)  
git log --oneline --graph --decorate --all  
git checkout develop  
git merge -m "merge feature to dev" feature/new-site  
![Second merge](second_merge.png)  
git log --oneline --graph --decorate --all  
nano nginx.conf  
git add .  
git commit -m "updated server path"  
git checkout feature/new-site  
git checkout develop  
git merge -m "merge feature to dev" feature/new-site  
![Merge conflict](merge_conflict.png)  
git checkout feature/new-site  
![Before Squash](before_squash.png)  
nano conf.d/mysite.domain.com.conf  
git add .  
git commit -m "added access log"  
nano conf.d/mysite.domain.com.conf  
git add .  
git commit -m "added error log"  
nano conf.d/mysite.domain.com.conf  
git add .  
git commit -m "added gzip"  
nano conf.d/mysite.domain.com.conf  
git add .  
git commit -m "added gzip types"  
git log --oneline --graph --decorate --all  
git rebase -i HEAD~4  
git rebase -i master  
![After Squash](after_squash.png)  
git log --oneline --graph --decorate --all  
git checkout develop  
![Before Cherry-pick](before_cherry_pick.png)  
nano nginx.conf  
git add .  
git commit -m "Formatted code"  
nano nginx.conf  
git add .  
git commit -m "Formatted code 2"  
nano nginx.conf  
git add .  
git commit -m "Formatted code 3"  
git log --oneline --graph --decorate --all  
git cherry-pick ffff745  
git add .  
git commit -m "Formatted code"   
git checkout master  
git log --oneline --graph --decorate --all  
git cherry-pick 8f6d06c  
git add .  
git commit -m "cherry-pick from"  
![After Cherry-pick](after_cherry_pick.png)  
git log --oneline --graph --decorate --all  
